
from setuptools import setup

setup(
    name='ansible_serial_shell',
    version='0.1',
    packages=['ansible_serial_shell'],
    license='MIT',
    entry_points = {
        'console_scripts': ['ansible_serial_shell=ansible_serial_shell.shell:repl']
    }
)
