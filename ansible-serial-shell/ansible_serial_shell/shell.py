
import sys
import subprocess
import binascii
import hashlib


def _runcmd(cmd, stdin, become_user=None, become_pass=None):
    subprocess.run('sudo -K', input=stdin, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if become_user is not None and become_pass is not None:
        cmd = 'sudo -u {} -S {}'.format(become_user, cmd)
        stdin = become_pass.encode() + b'\n' + stdin
    proc = subprocess.run(cmd, input=stdin, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stderr = proc.stderr
    if stderr.startswith(b'[sudo] password for') and b'\n' in stderr:
        _, stderr = stderr.split(b'\n', 1)
        if stderr.startswith(b'[sudo] password for'):
            raise PermissionError()
    return proc.returncode, proc.stdout, proc.stderr


def _format_runcmd(retval, stdout, stderr):
    print('=== BEGIN BASE64 STDOUT ===', end='\r\n')
    print(binascii.b2a_base64(stdout).decode().strip(), end='\r\n')
    print('=== BEGIN BASE64 STDERR ===', end='\r\n')
    print(binascii.b2a_base64(stderr).decode().strip(), end='\r\n')
    print('=== BEGIN RETURN VALUE ===', end='\r\n')
    print(retval, end='\r\n')
    print('=== END RETURN VALUE ===', end='\r\n')


def _put(filename):
    line = sys.stdin.readline().strip()
    while line != '=== BEGIN BASE64 FILE ===':
        line = sys.stdin.readline()
    line = sys.stdin.readline().strip()
    localhash = hashlib.sha256()
    with open(filename, 'wb') as fd:
        while line != '=== END BASE64 FILE ===':
            chunk = binascii.a2b_base64(line)
            localhash.update(chunk)
            fd.write(chunk)
            line = sys.stdin.readline().strip()
    print('=== BEGIN SHA256SUM ===', end='\r\n')
    print(localhash.hexdigest(), end='\r\n')
    print('=== END SHA256SUM ===', end='\r\n')


def _get(filename):
    print('=== BEGIN BASE64 FILE ===', end='\r\n')
    localhash = hashlib.sha256()
    with open(filename, 'rb') as fd:
        chunk = None
        while chunk is None or len(chunk) == 1023:
            chunk = fd.read(1023)
            localhash.update(chunk)
            print(binascii.b2a_base64(chunk).decode().strip(), end='\r\n')
    print('=== BEGIN SHA256SUM ===', end='\r\n')
    print(localhash.hexdigest(), end='\r\n')
    print('=== END SHA256SUM ===', end='\r\n')


def repl():
    try:
        while True:
            cmd, *arglist = sys.stdin.readline().split(' ', 1)
            cmd = cmd.strip()
            if len(arglist) > 0:
                arg = arglist[0].strip()
            else:
                arg = ''

            if cmd == 'version':
                print('ansible_serial_shell 0.1', end='\r\n')
            elif cmd == 'exit':
                sys.exit(0)
            elif cmd == 'runcmd':
                line = ''
                become_user = None
                become_pass = None
                read_become = False
                while line != '=== BEGIN BASE64 STDIN ===':
                    if read_become:
                        bline = binascii.a2b_base64(line).decode()
                        become_user, become_pass = bline.split(':', 1)
                        read_become = False
                    if line == '=== BEGIN BASE64 BECOME ===':
                        read_become = True
                    line = sys.stdin.readline().strip()
                stdin = ''
                while True:
                    line = sys.stdin.readline().strip()
                    if line == '=== END BASE64 STDIN ===':
                        break
                    stdin += line
                try:
                    retval, stdout, stderr = _runcmd(arg, binascii.a2b_base64(stdin), become_user, become_pass)
                    _format_runcmd(retval, stdout, stderr)
                except PermissionError:
                    print('=== BECOME FAILED ===')
            elif cmd == 'put':
                _put(arg)
            elif cmd == 'get':
                _get(arg)
            else:
                print('Invalid command: {}'.format(cmd), end='\r\n')
    except KeyboardInterrupt:
        pass


if __name__ == '__main__':
    repl()
