# Ansible Serial Connection Plugin

**WARNING:** This is probably the worst Ansible plugin that was ever written!
Use at your own risk.


**WARNING:** This project is WIP.

## Installation

### Control Machine

Copy `serial.py` into a `connection_plugins` directory next to your playbook.

### Target Machine

Install `ansible_serial_shell` system-wide, e.g. by running
`sudo python3 setup.py install`.  Create a user for ansible to log in with and
set its login shell to `/usr/local/bin/ansible_serial_shell`, and allow this
user to elevate priviliges using sudo.

## Usage

Example inventory:

    target ansible_connection=serial ansible_host=/dev/ttyUSB0 ansible_port=115200 ansible_user=ansible
    
Additionally, the variable `ansible_password` needs to be set, e.g. via a 
vault.  This plugin abuses the port variable as baud rate, other configuration
of the TTY is currently not supported.
