from typing import Tuple

from ansible.plugins.connection import ConnectionBase
from ansible.errors import AnsibleConnectionFailure, AnsibleActionFail

import serial
import binascii
import hashlib
from time import sleep

try:
    from __main__ import display
except ImportError:
    from ansible.utils.display import Display
    display = Display()

DOCUMENTATION = """
    author: s3lph
    connection: serial
    short_description: Run tasks via a serial console
    description:
      - Run tasks via a serial console
    version_added: 0.1
    options:
      serial_tty:
          type: string
          default: inventory_hostname
          description:
            - The host to connect to
          vars:
            - name: serial_tty
      baudrate:
          type: integer
          default: 115200
          description:
            - The TTYs baud rate
          vars:
            - name: baudrate
      user:
          type: string
          description:
            - The remote system user
          vars:
            - name: ansible_user
      password:
          type: string
          description:
            - The remote system password
          vars:
            - name: ansible_pass
"""


class Connection(ConnectionBase):

    transport = 'serial'
    has_pipelining = False
    become_methods = ['sudo']

    def __init__(self, play_context, new_stdin, *args, **kwargs):
        super().__init__(play_context, new_stdin, *args, **kwargs)
        self._tty: str = self._play_context.remote_addr
        self._baudrate: int = int(self._play_context.port)
        self._username: str = self._play_context.remote_user
        self._password: str = self._play_context.password
        self._become_user: str = self._play_context.become_user
        self._become_pass: str = self._play_context.become_pass
        self._console: serial.Serial = None
        self._connected: bool = False

    @staticmethod
    def _parse_ansible_shell_output(raw_out: bytes) -> Tuple[int, bytes, bytes]:
        lines = raw_out.split(b'\r\n')
        mode: int = 0
        stdout: bytes = b''
        stderr: bytes = b''
        retval: int = 0
        for line in lines:
            line = line.strip()
            if mode == 0:
                if line == b'=== BEGIN BASE64 STDOUT ===':
                    mode = 1
                continue
            elif mode == 1:
                if line != b'=== BEGIN BASE64 STDERR ===':
                    stdout += line
                else:
                    mode = 2
                continue
            elif mode == 2:
                if line != b'=== BEGIN RETURN VALUE ===':
                    stderr += line
                else:
                    mode = 3
                continue
            elif mode == 3:
                if line != b'=== END RETURN VALUE ===':
                    retval = int(line.decode())
                else:
                    break
        return retval, binascii.a2b_base64(stdout), binascii.a2b_base64(stderr)

    def _connect(self):
        super()._connect()
        if not self._connected:
            self._console = serial.Serial(self._tty, self._baudrate)
            self._console.read_all()
            self._console.write(b'\x03\x03\x03\x04\x04\x04\x04\x04')
            sleep(5)
            self._console.write(f'{self._username}\n'.encode())
            sleep(2)
            self._console.write(f'{self._password}\n'.encode())
            sleep(2)
            self._console.read_all()
            self._console.write('version\n'.encode())
            self._console.readline()
            sleep(1)
            line = self._console.read_all()
            if b'ansible_serial_shell' not in line:
                raise AnsibleConnectionFailure()
            self._connected = True

    def exec_command(self, cmd: str, in_data: str = None, sudoable: bool = True):
        super().exec_command(cmd, in_data, sudoable)
        self._console.write(f'runcmd {cmd}\n'.encode())
        if sudoable and self._become_user is not None and self._become_pass is not None:
            self._console.write(b'=== BEGIN BASE64 BECOME ===\n')
            creds: bytes = binascii.b2a_base64((self._become_user + ":" + self._become_pass).encode()) + b'\n'
            self._console.write(creds)
        self._console.write(b'=== BEGIN BASE64 STDIN ===\n')
        if in_data is not None:
            self._console.write(f'{binascii.b2a_base64(in_data.encode())}\n'.encode())
        self._console.write(b'=== END BASE64 STDIN ===\n')
        raw_out: bytes = self._console.readline().strip()
        while not raw_out.strip() == b'=== BEGIN BASE64 STDOUT ===':
            raw_out = self._console.readline()
            if raw_out == b'=== BECOME FAILED ===':
                raise AnsibleActionFail('Become failed')

        while not raw_out.strip().endswith(b'=== END RETURN VALUE ==='):
            sleep(.1)
            line: bytes = self._console.readline()
            raw_out += line
        return Connection._parse_ansible_shell_output(raw_out)

    def put_file(self, in_path: str, out_path: str):
        super().put_file(in_path, out_path)
        self._console.write(f'put {out_path}\n'.encode())
        localhash = hashlib.sha256()
        with open(in_path, 'rb') as infile:
            self._console.write(b'=== BEGIN BASE64 FILE ===\n')
            chunk: bytes = None
            while chunk is None or len(chunk) == 1023:
                chunk: bytes = infile.read(1023)
                localhash.update(chunk)
                self._console.write(binascii.b2a_base64(chunk) + b'\n')
            self._console.write(b'=== END BASE64 FILE ===\n')
        line = self._console.readline().strip()
        while line != b'=== BEGIN SHA256SUM ===':
            line = self._console.readline().strip()
        clientdigest: bytes = b''
        line = self._console.readline().strip()
        while line != b'=== END SHA256SUM ===':
            clientdigest = line
            line = self._console.readline().strip()
        localdigest: bytes = localhash.hexdigest().encode()
        if clientdigest != localdigest:
            raise AnsibleActionFail('put_file failed: Wrong checksum')
        return

    def fetch_file(self, in_path, out_path):
        super().fetch_file(in_path, out_path)
        self._console.write(f'get {in_path}\n'.encode())
        line: bytes = self._console.readline().strip()
        localhash = hashlib.sha256()
        remotedigest: bytes = b''
        with open(out_path, 'wb') as fd:
            while line != b'=== BEGIN BASE64 FILE ===':
                line = self._console.readline().strip()
            line: bytes = self._console.readline().strip()
            while line != b'=== BEGIN SHA256SUM ===':
                chunk: bytes = binascii.a2b_base64(line)
                localhash.update(chunk)
                fd.write(chunk)
                line: bytes = self._console.readline().strip()
                print(line)
            line: bytes = self._console.readline().strip()
            while line != b'=== END SHA256SUM ===':
                remotedigest = line
                line: bytes = self._console.readline().strip()
                print(line)
            localdigest: bytes = localhash.hexdigest().encode()
        if localdigest != remotedigest:
            raise AnsibleActionFail('fetch_file failed: Wrong checksum')

    def close(self):
        super().close()
        self._connected = False
        self._console.write(b'\x03')
        sleep(5)
        self._console.close()
